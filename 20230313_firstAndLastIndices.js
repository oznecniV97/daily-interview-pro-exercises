/*
Given a sorted array, A, with possibly duplicated elements,
find the indices of the first and last occurrences of a target element, x.
Return -1 if the target is not found.

Example:
Input: A = [1,3,3,5,7,8,9,9,9,15], target = 9
Output: [6,8]

Input: A = [100, 150, 150, 153], target = 150
Output: [1,2]

Input: A = [1,2,3,4,5,6,10], target = 9
Output: [-1, -1]
*/

const NO_OCCURRENCE = -1;

solution = function(arr, x) {

    let firstOccurence = NO_OCCURRENCE;

    for(let i = 0; i<arr.length; i++) {
        const num = arr[i];
        if(num === x) {
            if(firstOccurence === NO_OCCURRENCE) {
                firstOccurence = i;
            }
        } else {
            if(firstOccurence !== NO_OCCURRENCE) {
                return [firstOccurence, i-1];
            }
        }
    }

    //target not found case
    return [NO_OCCURRENCE, NO_OCCURRENCE];
}

//7m 34s

// Test program
const arr = [1, 2, 2, 2, 2, 3, 4, 7, 8, 8];
const x = 2;
console.log(solution(arr, x));
// [1, 4]
