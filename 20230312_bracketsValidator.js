/*
Imagine you are building a compiler.
Before running any code, the compiler must check that the parentheses in the program are balanced.
Every opening bracket must have a corresponding closing bracket.
We can approximate this using strings.

Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
An input string is valid if:
- Open brackets are closed by the same type of brackets.
- Open brackets are closed in the correct order.
- Note that an empty string is also considered valid.

Example:
Input: "((()))"
Output: True

Input: "[()]{}"
Output: True

Input: "({[)]"
Output: False
*/

const openBrackets = ["(", "[", "{"];
const closeBrackets = [")", "]", "}"];

solution = function(str) {
    const opened = [];

    for(let i = 0; i<str.length; i++) {
        const char = str[i];
        //se parentesi aperta, inserisco nell'array come ultimo elemento
        if(openBrackets.includes(char)) {
            opened.push(char);
        } else if(closeBrackets.includes(char)) {
            //se l'ultima parentesi aperta viene chiusa dalla giusta parentesi, rimuovo l'ultima apertura dall'array
            const lastOpened = opened[opened.length-1];
            if(openBrackets.indexOf(lastOpened) === closeBrackets.indexOf(char)) {
                opened.pop();
            } else {
                return false;
            }
        }
    }
    return opened.length === 0;
}

//10m 32s

// Test program
let text = '()(){(())';
console.log(solution(text));
// False
text = '';
console.log(solution(text));
// True
text = '([{}])()';
console.log(solution(text));
// True
