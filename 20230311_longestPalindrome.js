/*
A palindrome is a sequence of characters that reads the same backwards and forwards. Given a string, s, find the longest palindromic substring in s.

Example:
Input: "banana"
Output: "anana"

Input: "million"
Output: "illi"

Input: "onemilliondollarbaby"
Output: "illi"
*/

solution = function(str) {
    let maxPalindrome = "";
    //for each character, find an equal one from the end
    for(let startI = 0; startI<str.length; startI++) {
        const startChar = str[startI];
        for(let endI = str.length - 1; endI > startI; endI--) {
            const endChar = str[endI];
            if(startChar === endChar) {
                //check if the reverse of the found string is equals to the original one
                const found = str.substring(startI, endI+1);
                const reverse = found.split("").reverse().join("");
                if(found === reverse && maxPalindrome.length < found.length) {
                    maxPalindrome = found;
                }
            }
        }
    }
    return maxPalindrome;
}

// Test program
const text = 'tracecars';
console.log(solution(text));
// racecar
