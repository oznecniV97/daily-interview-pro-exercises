/*
An array of integers a is called smooth if the absolute value of the difference between any two adjacent elements is less than or equal to 1. You are given an array a. Check whether it is smooth and output "YES" if it is, or "NO" if it is not (quotes for clarity only).

The first line of the input contains an integer N - the number of elements in the array. 1 <= N <= 10.
The second line of the input contains N integers - the elements of the array, separated with single spaces. 1 <= ai <= 10.

Example

input

5
3 4 4 5 4

output

YES

input

4
1 1 8 2

output

NO
*/

// Installed node modules: jquery underscore request
// express jade shelljs passport http sys lodash async
// mocha moment connect validator restify ejs ws co
// when helmet wrench brain mustache should backbone forever debug

// functions
function checkEmptyLine(line: string): void {
  if (!line) {
    throw new Error("Empty input line not allowed.");
  }
}

function castNumber(s: string): number {
  const n = Number(s);
  if (isNaN(n)) {
    throw new Error(`Number required. Given '${s}'`);
  }
  return n;
}

function isSmoothArray(size: number, elements: number[]): boolean {
  for (let i = 1; i < size; i++) {
    if (Math.abs(elements[i] - elements[i - 1]) > 1) {
      return false;
    }
  }
  return true;
}

function printIsSmooth(size: number, arrayS: string): void {
  // convert string array into array
  const elements = arrayS.split(" ").map(castNumber);
  // check array size equals to size
  if (elements.length != size) {
    throw new Error(`Input size different than element number (${size}, ${elements.length}).`);
  }
  // check if array is smooth and print result
  console.log(isSmoothArray(size, elements) ? "YES" : "NO");
}

// execution
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: ''
});

rl.prompt();

let elemSize: number;
rl.on('line', (line: any) => {
  checkEmptyLine(line);
  //first line case
  if (!elemSize) {
    elemSize = castNumber(line);
    return;
  }

  printIsSmooth(elemSize, line);
});

rl.on('close', () => {
  process.exit(0);
});