/*
To see if a number is divisible by 3, you need to add up the digits of its decimal notation, and check if the sum is divisible by 3.
To see if a number is divisible by 11, you need to split its decimal notation into pairs of digits (starting from the right end), add up corresponding numbers and check if the sum is divisible by 11.  

For any prime p (except for 2 and 5) there exists an integer r such that a similar divisibility test exists: to check if a number is divisible by p, you need to split its decimal notation into r-tuples of digits (starting from the right end), add up these r-tuples and check whether their sum is divisible by p.  

Given a prime int p, find the minimal r for which such divisibility test is valid and output it.

The input consists of a single integer p - a prime between 3 and 999983, inclusive, not equal to 5.

Example

input

3

output

1

input

11

output

2
*/

// Installed node modules: jquery underscore request
// express jade shelljs passport http sys lodash async
// mocha moment connect validator restify ejs ws co
// when helmet wrench brain mustache should backbone forever debug

// functions
function checkEmptyLine(line: string): void {
  if (!line) {
    throw new Error("Empty input line not allowed.");
  }
}

function castNumber(s: string): number {
  const n = Number(s);
  if (isNaN(n)) {
    throw new Error(`Number required. Given '${s}'`);
  }
  return n;
}

function checkInputNumber(n: number): void {
  if (n < 3) {
    throw new Error(`Number lower than 3 are not allowed. (${n})`);
  }
  if (n === 5) {
    throw new Error("Number 5 is not allowed.");
  }
  if (n > 999983) {
    throw new Error(`Number higher than 999983 are not allowed. (${n})`);
  }
}

function findMinimalTupleSize(primeN: number): number {
  let size = 1;
  let remainder = 10 % primeN;

  while (remainder !== 1) {
      size++;
      remainder = (remainder * 10) % primeN;
  }

  return size;
}

// execution
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: ''
});

rl.prompt();

rl.on('line', (line: any) => {
  checkEmptyLine(line);
  const num = castNumber(line);
  checkInputNumber(num);
  const size = findMinimalTupleSize(num);
  console.log(size);
});

rl.on('close', () => {
  process.exit(0);
});