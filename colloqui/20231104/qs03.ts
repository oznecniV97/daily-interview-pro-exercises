/*
Write a program that takes input of an integer N, followed by N more integers. The program then outputs the longest increasing subsequence present in that sequence. If more than 1 sequence exists, output the earliest one.

Example

Input
5
6 1 5 2 9

Output
1 5 9
*/

// Installed node modules: jquery underscore request
// express jade shelljs passport http sys lodash async
// mocha moment connect validator restify ejs ws co
// when helmet wrench brain mustache should backbone forever debug

// functions
function checkEmptyLine(line: string): void {
  if (!line) {
    throw new Error("Empty input line not allowed.");
  }
}

function castNumber(s: string): number {
  const n = Number(s);
  if (isNaN(n)) {
    throw new Error(`Number required. Given '${s}'`);
  }
  return n;
}

function findIncreasingSequence(size: number, elements: number[]): number[] {
  const lisLength: number[] = Array(size).fill(1);
  const previousIndex: number[] = Array(size).fill(-1);

  for (let i = 0; i < size; i++) {
    for (let j = 0; j < i; j++) {
      if (elements[i] > elements[j] && lisLength[i] < lisLength[j] + 1) {
        lisLength[i] = lisLength[j] + 1;
        previousIndex[i] = j;
      }
    }
  }

  let maxLength = Math.max(...lisLength);
  let endIndex = lisLength.indexOf(maxLength);

  const longestIncreasingSubsequence: number[] = [];
  while (endIndex >= 0) {
    longestIncreasingSubsequence.unshift(elements[endIndex]);
    endIndex = previousIndex[endIndex];
  }

  return longestIncreasingSubsequence;
}

function printSequence(size: number, arrayS: string): void {
  // convert string array into array
  const elements = arrayS.split(" ").map(castNumber);
  // check array size equals to size
  if (elements.length != size) {
    throw new Error(`Input size different than element number (${size}, ${elements.length}).`);
  }
  // find longest sequence
  const sequence = findIncreasingSequence(size, elements);
  // print sequence
  console.log(sequence.join(" "));
}

// execution
const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
  prompt: ''
});

rl.prompt();

let elemSize: number;
rl.on('line', (line: any) => {
  checkEmptyLine(line);
  //first line case
  if (!elemSize) {
    elemSize = castNumber(line);
    return;
  }

  printSequence(elemSize, line);
});

rl.on('close', () => {
  process.exit(0);
});
