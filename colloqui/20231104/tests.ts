/*
An array of integers a is called smooth if the absolute value of the difference between any two adjacent elements is less than or equal to 1.
You are given an array a. Check whether it is smooth and output "YES" if it is, or "NO" if it is not (quotes for clarity only).

The first line of the input contains an integer N - the number of elements in the array. 1 <= N <= 10.
The second line of the input contains N integers - the elements of the array, separated with single spaces. 1 <= ai <= 10.

Example
input
5
3 4 4 5 4
output
YES

input
4
1 1 8 2

output
NO
 */

// function findMinDivisibilityGpt(primeN: number): number {
//   let r = 1;
//   let remainder = 10 % primeN;
//   while (remainder !== 1) {
//     r++;
//     remainder = (remainder * 10) % primeN;
//   }
//   return r;
// }


//to check if a number is divisible by p, you need to split its decimal notation into r-tuples of digits (starting from the right end),
//add up these r-tuples and check whether their sum is divisible by p.
/*
function checkDivisibility(num: number, prime: number, tuplesN: number): boolean {
  const sNum = num + "";
  for(let i = sNum.length - 1; i >= 0; i--) {
    
  }
  return false;
}

function findMinDivisibility(primeN: number): number {
  let r = 1;
  let remainder = 10 % primeN;

  while (remainder !== 1) {
    r++;
    remainder = (remainder * 10) % primeN;
  }

  return r;
}

const num = 999983;
console.log(findMinDivisibility(num));
*/

//5, 9, 2, 6, 7
//5, 9
//5, 6, 7

//6, 1, 4, 5, 9, 3, 4, 5, 9, 2, 6, 7
//1, 3, 4, 5, 6, 7

//KO
//1, 4, 5, 9

function findIncreasingSequence(size: number, elements: number[]): number[] {
  //TODO: possibile soluzione: per ogni numero iniziare a creare un albero con i numeri successivi che sono maggiori di sé stesso ciclicamente, poi andare a prendere il ramo più lungo
  let maxSeq: number[] = [elements[0]];

  

  return maxSeq;

  // //let sequence = [elements[0]];
  // let maxSeq: number[] = [];
  // let sequence: number[] = [];
  // for(let i = 0; i < size; i++) {
  //   let minNum = elements[i];
  //   //definisco come primo elemento il numero iniziale (che è anche il minimo)
  //   sequence = [minNum];

  //   //ciclo per il resto dei numeri
  //   for(let j = i + 1; j < size; j++) {
  //     const elem = elements[j];

  //     //se il numero j è maggiore dell'ultimo minimo, lo aggiungo alla sequenza e lo prendo come prossimo minimo
  //     if(elem > minNum) {
  //       sequence.push(elem);
  //       minNum = elem;
  //     }
  //   }

  //   //controllo se la sequenza generata è più lunga della massima
  //   if(sequence.length > maxSeq.length) {
  //     maxSeq = sequence;
  //   }
  // }
  // return maxSeq;
}

function printRes(numA: number[][], resA: number[][], expA: string[]) {
  const num = numA.pop();
  const res = resA.pop()?.join(" ");
  const exp = expA.pop();
  console.log(`input: ${num}`);
  console.log(`response: ${res}`);
  console.log(`expected: ${exp}`);
  console.log(`OK: ${exp === res}`);
}

const num: number[][] = [];
const res: number[][] = [];
const exp: string[] = [];

num.push([6, 1, 4, 5, 2, 9]);
res.push(findIncreasingSequence(num[num.length - 1].length, num[num.length - 1]));
exp.push("1 4 5 9");
printRes(num, res, exp);

num.push([6, 1, 4, 5, 9, 3, 4, 5, 9, 2, 6, 7]);
res.push(findIncreasingSequence(num[num.length - 1].length, num[num.length - 1]));
exp.push("1 3 4 5 6 7");
printRes(num, res, exp);

//1184 1537 2808 3323 6116 6968 7084 7944 10386 11873 12647 13062 13252 18416 19489 21357 23150 23762 23854 24445 24938 25699 26318 26347 26369 26436 27216 27879 29613 29830 30259
//console.log(findIncreasingSequence(256, 
//  "4650 2543 1184 1537 10037 9856 18201 29781 16440 8124 15835 23273 21808 2808 28925 2374 19 16546 9279 3323 19905 14701 20381 6116 6968 18094 1572 7084 21256 10758 16133 16017 7944 20546 13544 3431 25158 13183 20354 3808 3908 10386 19306 7178 11873 7079 5160 8729 10389 39 1167 12647 10849 30688 334 32185 13062 30048 8641 13252 9206 23969 23309 18416 25736 27884 24939 8795 28143 25335 13252 19489 21357 14929 1407 475 6852 28415 31383 20325 30958 6248 27120 6373 18189 23150 6635 13576 17317 29150 11064 31887 29705 24586 9707 29451 14867 31156 23762 27876 29208 23854 6918 22176 11547 5056 20417 354 23361 25162 11375 6321 31365 3370 26888 9595 7597 4960 8860 16986 24445 9456 2096 25877 5674 22664 18888 24938 23314 25699 32197 2315 15017 24173 28785 3302 8375 18344 4569 937 21697 26318 30171 14726 6852 13880 6783 11572 19498 26878 26763 24118 31990 19296 8785 20575 322 613 13516 26774 26347 28353 7915 1751 14219 14076 25389 378 5799 15569 14190 26040 4138 469 26369 8555 11026 16217 6910 31253 5226 13712 31965 29988 30328 30197 26436 5492 20535 11495 6430 4630 24840 11926 7917 27216 31908 19235 26209 31460 23381 11779 12329 30869 14950 19621 21513 29672 24791 10324 2861 18984 23638 26462 13501 10156 2667 2422 18609 32605 16883 15191 27879 7026 12495 3498 24690 11134 16861 4906 12598 29613 7975 10758 28594 330 9307 29830 21788 2181 21884 17650 4019 20340 11734 176 10577 19031 23351 16044 15423 19744 14684 9469 27214 30259".split(" ").map(Number)));
