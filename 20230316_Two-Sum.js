/*
You are given a list of numbers, and a target number k. Return whether or not there are two numbers in the list that add up to k.
Try to do it in a single pass of the list.

Example:
Given [4, 7, 1 , -3, 2] and k = 5,
return true since 4 + 1 = 5.
*/

solution = function(list, k) {
    for (let i=0; i < (list.length-1) ;i++) {
        const num = list[i];
        for(let j=i+1; j < list.length; j++) {
            const secNum = list[j];
            if((num + secNum) === k) {
                return true;
            }
        }
    }
    return false;
}

// Test program
console.log(solution([4,7,1,-3,2], 5));
// true
