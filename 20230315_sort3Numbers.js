/*
Given a list of numbers with only 3 unique numbers (1, 2, 3), sort the list in O(n) time.

Example 1:
Input: [3, 3, 2, 1, 3, 2, 1]
Output: [1, 1, 2, 2, 3, 3, 3]
*/

solution = function(nums) {
    let valuesCount = [0, 0, 0];
    nums.forEach(num => valuesCount[num-1]++);

    const newArr = [];
    for(let i = 0; i < 3; i++) {
        for(let j = 0; j < valuesCount[i]; j++) {
            newArr.push(i+1);
        }
    }
    return newArr;
}

// Test program
console.log(solution([3, 3, 2, 1, 3, 2, 1]));
// [1, 1, 2, 2, 3, 3, 3]
