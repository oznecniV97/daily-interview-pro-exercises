/*
You are given two linked-lists representing two non-negative integers.
The digits are stored in reverse order and each of their nodes contain a single digit.
Add the two numbers and return it as a linked list.

Example:
Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
Output: 7 -> 0 -> 8
Explanation: 342 + 465 = 807.
*/

// Definition for a linked-list node.
class Node {
    val;
    next;
    constructor(val, next=null) {
        this.val = val;
        this.next = next;
    }
}

inverseSumTwoLinkedListRecursive = function(a, b, resto = 0) {
    //end case
    if(!a || !b) {
        return null;
    }

    //sum values
    const sum = a.val + b.val + resto;
    
    //split units and decine
    let realDigit;
    resto = 0;
    if(sum>9) {
        const sumString = sum.toString();
        realDigit = Number(sumString.charAt(1));
        resto = Number(sumString.charAt(0));
    } else {
        realDigit = sum;
    }

    return new Node(realDigit, inverseSumTwoLinkedListRecursive(a.next, b.next, resto));
}

inverseSumTwoLinkedList = function(extA, extB) {
    let a = extA;
    let b = extB;
    let head;
    let node;
    let resto = 0;
    while(a && b) {
        //sum values
        const sum = a.val + b.val + resto;
        
        //split units and decine
        let realDigit;
        resto = 0;
        if(sum>9) {
            const sumString = sum.toString();
            realDigit = Number(sumString.charAt(1));
            resto = Number(sumString.charAt(0));
        } else {
            realDigit = sum;
        }

        if(head) {
            node.next = new Node(realDigit);
            node = node.next;
        } else {
            node = new Node(realDigit);
            head = node;
        }
        a = a.next;
        b = b.next;
    }
    return head;
}

// Test program
// 2 -> 4 -> 3
const list = new Node(2);
list.next = new Node(4);
list.next.next = new Node(3);

// 5 -> 6 -> 4
const b = new Node(5)
b.next = new Node(6)
b.next.next = new Node(4)

let c = inverseSumTwoLinkedListRecursive(list, b);
while(c) {
    console.log(c.val)
    c = c.next
}

console.log(list);
console.log(b);

c = inverseSumTwoLinkedList(list, b);
while(c) {
    console.log(c.val)
    c = c.next
}

console.log(list);
console.log(b);
// 7 0 8
