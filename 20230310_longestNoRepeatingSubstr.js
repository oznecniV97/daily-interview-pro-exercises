/*
Given a string, find the length of the longest substring without repeating characters.
abrkaabcdefghijjxxx => 10 
*/

lengthOfLongestSubstring = function(str) {
    let max = 1;
    let count = 0;
    let lastChar;
    //check each character
    for(let i = 0; i<str.length; i++) {
        const char = str[i];
        //if char is equals to last, stop count
        if(char === lastChar) {
            if(max < count) {
                max = count;
            }
            count = 0;
        }
        ++count;
        lastChar = char;
    }
    return max;
}

// Test program
const text = 'abrkaabcdefghijjxxx';
console.log(lengthOfLongestSubstring(text));
// 10
