//Given two sorted linked lists, merge them in order.
// Definition for a linked-list node.
class Node {
    val;
    next;
    constructor(val, next=null) {
        this.val = val;
        this.next = next;
    }
}

mergeTwoListsRecursive = function(a, b) {
    if(!a) {
        return b;
    } else if(!b) {
        return a;
    }

    let newList;
    if(a.val < b.val) {
        newList = new Node(a.val);
        newList.next = mergeTwoListsRecursive(a.next, b);
    } else {
        newList = new Node(b.val);
        newList.next = mergeTwoListsRecursive(a, b.next);
    }

    return newList;
}

mergeTwoLists = function(a, b) {
    if(!a) {
        return b;
    } else if(!b) {
        return a;
    }

    let head;
    if(a.val < b.val) {
        head = new Node(a.val);
        a = a.next;
    } else {
        head = new Node(b.val);
        b = b.next;
    }

    let lastNode = head;
    while(a || b) {
        console.log("LOOOOOP");
        console.log("newList", head);
        console.log("a", a);
        console.log("b", b);
        
        if(!a) {
            lastNode.next = new Node(b.val);
            b = b.next;
        } else if(!b) {
            lastNode.next = new Node(a.val);
            a = a.next;
        } else if(a.val < b.val) {
            lastNode.next = new Node(a.val);
            a = a.next;
        } else {
            lastNode.next = new Node(b.val);
            b = b.next;
        }

        lastNode = lastNode.next;
    }

    return head;
}

mergeTwoListsOpt = function(a, b) {
    let head;
    let node;

    while(true) {
        let nextNode;
        if(!a) {
            nextNode = b;
        } else if(!b) {
            nextNode = a;
        } else if(a.val < b.val) {
            nextNode = a;
        } else {
            nextNode = b;
        }

        if(!nextNode) {
            break;
        }

        if(nextNode === a) {
            a = a.next;
        } else {
            b = b.next;
        }

        if(!head) {
            head = nextNode;
            node = nextNode;
        } else {
            node.next = nextNode;
            node = nextNode;
        }
    }
    return head;
}

// Test program
// 1 -> 3 ->5
const a = new Node(1);
a.next = new Node(3);
a.next.next = new Node(5);

// 2 -> 4 -> 6
const b = new Node(2)
b.next = new Node(4)
b.next.next = new Node(6)

//let c = mergeTwoLists(a, b);
//let c = mergeTwoListsRecursive(a, b);
let c = mergeTwoListsOpt(a, b);
while(c) {
    console.log(c.val)
    c = c.next
}
console.log("a",a);
console.log("b",b);
// 1 2 3 4 5 6