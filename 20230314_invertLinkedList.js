/*
Given a singly-linked list, reverse the list. This can be done iteratively or recursively. Can you get both solutions?

Example:
Input: 4 -> 3 -> 2 -> 1 -> 0 -> NULL
Output: 0 -> 1 -> 2 -> 3 -> 4 -> NULL
*/

// Definition for a linked-list node.
class Node {
    val;
    next;
    constructor(val, next=null) {
        this.val = val;
        this.next = next;
    }

    toString() {
        if(!this.next) {
            return this.val;
        }
        return this.val + " -> " + this.next.toString();
    }

    solutionRecursive() {
        if(!this.next || this.next.val > this.val) {
            return;
        }

        //invert number value
        const tmp = this.next.val;
        this.next.val = this.val;
        this.val = tmp;

        //call method on next element to move original val to the end
        this.next.solutionRecursive();

        //recursive call to move every number
        this.solutionRecursive();
    }

    solutionIteratively() {
        if(!this.next) {
            return;
        }

        while(this.val < this.next.val) {
            //set pointer to start
            let pointer = this;
            //cicle to move pointer to last available position
            while(pointer.next && pointer.val < pointer.next.val) {
                //invert number value
                const tmp = pointer.next.val;
                pointer.next.val = pointer.val;
                pointer.val = tmp;

                //go to next pointer
                pointer = pointer.next;
            }
        }
    }
}

//

// Test program
const list = new Node(5);
list.next = new Node(4);
list.next.next = new Node(3);
list.next.next.next = new Node(2);
list.next.next.next.next = new Node(1);

console.log(list.toString());
// 5 -> 4 -> 3 -> 2 -> 1

list.solutionRecursive();

console.log(list.toString());
// 1 -> 2 -> 3 -> 4 -> 5

list.solutionIteratively();

console.log(list.toString());
// 5 -> 4 -> 3 -> 2 -> 1
