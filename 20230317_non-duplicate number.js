/*
Given a list of numbers, where every number shows up twice except for one number, find that one number.

Example:
Input: [4, 3, 2, 4, 1, 3, 2]
Output: 1

Challenge: Find a way to do this using O(1) memory.
*/

//linear way -> O(n) memory
linearSolution = function(nums) {
    const single = {};
    for (const num of nums) {
        if(single[num]) {
            delete single[num]; 
        }
        single[num] = 1;
    }
    const keys = Object.keys(single);
    if(keys.length > 1) {
        throw "More than one single number: " + keys;
    }
    return keys[0];
}

//constant way -> O(1) memory
//fa cacare però a livello di operazioni
solution = function(nums) {
    for (let i = 0; i < nums.length; i++) {
        const num = nums[i];
        let found = false;
        for (let j = 0; j < nums.length; j++) {
            const comparison = nums[j];
            if(i !== j && num === comparison) {
                found = true;
                break;
            }
        }
        if(!found) {
            return num;
        }
    }
}

// Test program
console.log(solution([4, 3, 2, 4, 1, 3, 2]));
// 1
