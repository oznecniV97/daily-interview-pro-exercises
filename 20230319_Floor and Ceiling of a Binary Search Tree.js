/*
Given an integer k and a binary search tree, find the floor (less than or equal to) of k, and the ceiling (larger than or equal to) of k.
If either does not exist, then print them as undefined.
*/

class Node {
    left;
    right;
    value;

    constructor(value) {
        this.value = value;
    }
}

solution = function(rootNode, k, floor, ceil) {
    const value = rootNode.value;

    if(value === k) {
        return [value, value];
    }

    let nextNode;
    if(value < k && (!floor || value > floor)) {
        //save value as floor (less than or equal to)
        floor = value;

        //check left nodes to find more precise ceil and floor
        nextNode = rootNode.right;
    } else if(value > k && (!ceil || value < ceil)) {
        //save value as ceil (larger than or equal to)
        ceil = value;

        //check left nodes to find more precise ceil and floor
        nextNode = rootNode.left;
    }

    if(nextNode) {
        return solution(nextNode, k, floor, ceil);
    }

    return [floor, ceil];
}

const root = new Node(8);
root.left = new Node(4);
root.right = new Node(12);
  
root.left.left = new Node(2);
root.left.right = new Node(6);
  
root.right.left = new Node(10);
root.right.right = new Node(14);

// Test program
console.log(solution(root, 5));
// [4, 6]
console.log(solution(root, 10));
// [10, 10]
console.log(solution(root, 18));
// [14, undefined]
